<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Interfaces\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    public function suggested($userIds)
    {
        return User::whereNotIn('id', $userIds)->paginate(10);
    }

    public function followed($userIds)
    {
        return User::whereIn('id', $userIds)->paginate(10);
    }

    public function create($payload)
    {
        $user = new User();
        $user->name = $payload->name;
        $user->email = $payload->email;
        $user->password = Hash::make($payload->password);
        $user->save();

        return $user->fresh();
    }

    public function showByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function showById($id)
    {
        return User::findOrFail($id);
    }
}
