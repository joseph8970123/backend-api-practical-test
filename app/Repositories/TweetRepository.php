<?php

namespace App\Repositories;

use App\Interfaces\Repositories\TweetRepositoryInterface;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use App\Models\Tweet;

class TweetRepository implements TweetRepositoryInterface
{
    public function index()
    {
        return Tweet::where('user_id', auth()->user()->id)->paginate(10);
    }

    public function showById($id)
    {
        return Tweet::findOrFail($id);
    }

    public function create($payload)
    {
        $tweet = new Tweet();
        $tweet->user_id = auth()->user()->id;
        $tweet->content = $payload->content;
        $tweet->save();

        if ($payload->attachment) {
            $this->uploadMedia($payload, $tweet->id);
        }

        return $tweet->fresh();
    }

    public function update($payload, $id)
    {
        $tweet = Tweet::findOrFail($id);
        $tweet->content = $payload->content;
        $tweet->save();

        if ($payload->attachment) {
            $this->uploadMedia($payload, $tweet->id);
        }

        return $tweet->fresh();
    }

    public function delete($id)
    {
        return Tweet::findOrFail($id)->delete();
    }

    public function uploadMedia($payload, $id)
    {
        $tweetMedia = Tweet::findOrFail($id);
        $media = Media::where('model_id', $tweetMedia->id)->where('model_type', 'App\Models\Tweet')->first();
        if ($media) {
            $media->forceDelete($media->id);
        }
        $tweetMedia->addMediaFromRequest('attachment')->toMediaCollection('tweet_media');
        $tweetMedia->attachment = $tweetMedia->getMedia('tweet_media')->last()->getUrl();
        $tweetMedia->save();

        return $tweetMedia->fresh();
    }
}
