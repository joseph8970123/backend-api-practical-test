<?php

namespace App\Repositories;

use App\Http\Resources\UserResource;
use App\Interfaces\Repositories\FollowedUserRepositoryInterface;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Models\FollowedUser;

class FollowedUserRepository implements FollowedUserRepositoryInterface
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function followedList()
    {
        $userId = auth()->user()->id;
        $followedIds = FollowedUser::where('user_id', $userId)->pluck('followed_user_id');

        return $this->userRepository->followed($followedIds);
    }

    public function suggestedList()
    {
        $userId = auth()->user()->id;
        $suggestedIds = FollowedUser::where('user_id', $userId)->pluck('followed_user_id');
        $suggestedIds->push($userId);

        return $this->userRepository->suggested($suggestedIds);
    }

    public function follow($id)
    {
        $follow = new FollowedUser();
        $follow->user_id = auth()->user()->id;
        $follow->followed_user_id = $id;
        $follow->save();

        return $follow->fresh();
    }

    public function unfollow($id)
    {
        $unfollow = FollowedUser::where('user_id', auth()->user()->id)
            ->where('followed_user_id', $id)
            ->first();

        return $unfollow->delete();
    }
}
