<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FollowedUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'followed_user_id'
    ];

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
