<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Tweet extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, InteractsWithMedia;

    protected $fillable = [
        'user_id',
        'content',
        'attachment'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('tweet_media')->useDisk('public');
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
