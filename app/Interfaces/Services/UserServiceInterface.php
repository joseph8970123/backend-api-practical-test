<?php

namespace App\Interfaces\Services;

interface UserServiceInterface
{
    public function createUser(object $payload);
}
