<?php

namespace App\Interfaces\Services;

interface AuthServiceInterface
{
    public function authenticateUser(object $payload);
    public function logoutUser(object $request);
}
