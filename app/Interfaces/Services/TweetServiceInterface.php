<?php

namespace App\Interfaces\Services;

interface TweetServiceInterface
{
    public function tweetList();
    public function getTweetById(int $id);
    public function createTweet(object $payload);
    public function updateTweet(object $payload, int $id);
    public function deleteTweet(int $id);
}
