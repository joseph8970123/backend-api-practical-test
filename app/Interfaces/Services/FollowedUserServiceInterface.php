<?php

namespace App\Interfaces\Services;

interface FollowedUserServiceInterface
{
    public function followedUsers();
    public function suggestedUsers();
    public function followUser(int $id);
    public function unfollowUser(int $id);
}
