<?php

namespace App\Interfaces\Repositories;

interface UserRepositoryInterface
{
    public function followed($userIds);
    public function suggested($userIds);
    public function create(object $payload);
    public function showByEmail(string $email);
    public function showById(int $id);
}
