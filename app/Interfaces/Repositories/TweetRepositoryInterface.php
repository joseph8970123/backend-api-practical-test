<?php

namespace App\Interfaces\Repositories;

interface TweetRepositoryInterface
{
    public function index();
    public function showById(int $id);
    public function create(object $payload);
    public function update(object $payload, int $id);
    public function delete(int $id);
}
