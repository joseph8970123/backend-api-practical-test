<?php

namespace App\Interfaces\Repositories;

interface FollowedUserRepositoryInterface
{
    public function followedList();
    public function suggestedList();
    public function follow(int $id);
    public function unfollow(int $id);
}
