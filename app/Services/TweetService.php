<?php

namespace App\Services;

use App\Http\Resources\TweetResource;
use App\Interfaces\Repositories\TweetRepositoryInterface;
use App\Interfaces\Services\TweetServiceInterface;

class TweetService implements TweetServiceInterface
{
    private $tweetRepository;

    public function __construct(TweetRepositoryInterface $tweetRepository)
    {
        $this->tweetRepository = $tweetRepository;
    }

    public function tweetList()
    {
        $tweets = $this->tweetRepository->index();

        return TweetResource::collection($tweets);
    }

    public function getTweetById($id)
    {
        $tweet = $this->tweetRepository->showById($id);

        return new TweetResource($tweet);
    }

    public function createTweet($payload)
    {
        $tweet = $this->tweetRepository->create($payload);

        return new TweetResource($tweet);
    }

    public function updateTweet($payload, $id)
    {
        $tweet = $this->tweetRepository->update($payload, $id);

        return new TweetResource($tweet);
    }

    public function deleteTweet($id)
    {
        return $this->tweetRepository->delete($id);
    }
}
