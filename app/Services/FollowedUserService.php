<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Interfaces\Repositories\FollowedUserRepositoryInterface;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Interfaces\Services\FollowedUserServiceInterface;

class FollowedUserService implements FollowedUserServiceInterface
{
    private $followedUserRepository;
    private $userRepository;

    public function __construct(FollowedUserRepositoryInterface $followedUserRepository, UserRepositoryInterface $userRepository)
    {
        $this->followedUserRepository = $followedUserRepository;
        $this->userRepository = $userRepository;
    }

    public function followedUsers()
    {
        $follows = $this->followedUserRepository->followedList();

        return UserResource::collection($follows);
    }

    public function suggestedUsers()
    {
        $user = $this->followedUserRepository->suggestedList();

        return UserResource::collection($user);
    }

    public function followUser($id)
    {
        $this->followedUserRepository->follow($id);
        $user = $this->userRepository->showById($id);

        return new UserResource($user);
    }

    public function unfollowUser($id)
    {
        $this->followedUserRepository->unfollow($id);
        $user = $this->userRepository->showById($id);

        return new UserResource($user);
    }
}
