<?php

namespace App\Providers;

use App\Interfaces\Repositories\FollowedUserRepositoryInterface;
use App\Interfaces\Repositories\TweetRepositoryInterface;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Interfaces\Services\AuthServiceInterface;
use App\Interfaces\Services\FollowedUserServiceInterface;
use App\Interfaces\Services\TweetServiceInterface;
use App\Interfaces\Services\UserServiceInterface;
use App\Repositories\FollowedUserRepository;
use App\Repositories\TweetRepository;
use App\Repositories\UserRepository;
use App\Services\AuthService;
use App\Services\FollowedUserService;
use App\Services\TweetService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //Repositories
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(TweetRepositoryInterface::class, TweetRepository::class);
        $this->app->bind(FollowedUserRepositoryInterface::class, FollowedUserRepository::class);

        //Services
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(AuthServiceInterface::class, AuthService::class);
        $this->app->bind(TweetServiceInterface::class, TweetService::class);
        $this->app->bind(FollowedUserServiceInterface::class, FollowedUserService::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
