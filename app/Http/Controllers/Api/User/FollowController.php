<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Interfaces\Services\FollowedUserServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowController extends Controller
{
    private $followedUserService;

    public function __construct(FollowedUserServiceInterface $followedUserService)
    {
        $this->followedUserService = $followedUserService;
    }

    public function followedList(): JsonResponse
    {
        return $this->followedUserService->followedUsers()->response();
    }

    public function suggestionList(): JsonResponse
    {
        return $this->followedUserService->suggestedUsers()->response();
    }

    public function follow($id): JsonResponse
    {
        return $this->followedUserService->followUser($id)->response();
    }

    public function unfollow($id): JsonResponse
    {
        return $this->followedUserService->unfollowUser($id)->response();
    }
}
