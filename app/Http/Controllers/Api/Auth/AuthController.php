<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserStoreRequest;
use App\Interfaces\Services\AuthServiceInterface;
use App\Interfaces\Services\UserServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $userService;
    private $authService;

    public function __construct(UserServiceInterface $userService, AuthServiceInterface $authService)
    {
        $this->userService = $userService;
        $this->authService = $authService;
    }

    public function store(UserStoreRequest $request): JsonResponse
    {
        $payload = (object) $request->only([
            'name',
            'email',
            'password'
        ]);

        return $this->userService->createUser($payload)->response();
    }

    public function login(LoginRequest $request): JsonResponse
    {
        $payload = (object) $request->only([
            'email',
            'password'
        ]);

        return $this->authService->authenticateUser($payload)->response();
    }

    public function logout(Request $request)
    {
        return $this->authService->logoutUser($request);
    }
}
