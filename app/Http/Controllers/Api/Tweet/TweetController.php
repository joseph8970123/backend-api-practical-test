<?php

namespace App\Http\Controllers\Api\Tweet;

use App\Http\Controllers\Controller;
use App\Http\Requests\TweetStoreRequest;
use App\Http\Requests\TweetUpdateRequest;
use App\Interfaces\Services\TweetServiceInterface;
use Illuminate\Http\JsonResponse;

class TweetController extends Controller
{
    private $tweetService;

    public function __construct(TweetServiceInterface $tweetService)
    {
        $this->tweetService = $tweetService;
    }

    public function index(): JsonResponse
    {
        return $this->tweetService->tweetList()->response();
    }

    public function show($id): JsonResponse
    {
        return $this->tweetService->getTweetById($id)->response();
    }

    public function store(TweetStoreRequest $request): JsonResponse
    {
        $payload = (object) $request->only([
            'content',
            'attachment'
        ]);

        return $this->tweetService->createTweet($payload)->response();
    }

    public function update(TweetUpdateRequest $request, $id): JsonResponse
    {
        $payload = (object) $request->only([
            'content',
            'attachment'
        ]);

        return $this->tweetService->updateTweet($payload, $id)->response();
    }

    public function destroy($id)
    {
        return $this->tweetService->deleteTweet($id);
    }
}
