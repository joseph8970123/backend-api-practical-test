<?php

namespace Database\Seeders;

use App\Models\Tweet;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TweetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 5; $i++) {
            $faker = Faker::create();
            $tweet = new Tweet();
            $tweet->user_id = $i;
            $tweet->content = $faker->paragraph();
            $tweet->attachment = $faker->imageUrl();
            $tweet->save();
        }
    }
}
