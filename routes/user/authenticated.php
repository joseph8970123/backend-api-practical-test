<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Tweet\TweetController;
use App\Http\Controllers\Api\User\FollowController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {

    Route::apiResources([
        'tweets' => TweetController::class,
    ]);

    Route::get('/follow/list', [FollowController::class, 'followedList']);
    Route::get('/suggested-users', [FollowController::class, 'suggestionList']);
    Route::post('/follow/{id}', [FollowController::class, 'follow']);
    Route::delete('/unfollow/{id}', [FollowController::class, 'unfollow']);

    Route::post('/tweets/update-tweet/{id}', [TweetController::class, 'update']);

    Route::post('/logout', [AuthController::class, 'logout']);
});
