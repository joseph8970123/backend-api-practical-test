<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    public function test_user_can_register()
    {
        $payload = [
            'name' => 'John Doe',
            'email' => 'johndoe@gmail.com',
            'password' => 'password'
        ];

        $response = $this->json('POST', '/api/user/auth/register', $payload);

        $response->assertStatus(200);
    }

    public function test_user_can_login()
    {
        $user = User::inRandomOrder()->first();

        $payload = [
            'email' => $user->email,
            'password' => 'password'
        ];

        $response = $this->postJson('/api/user/auth/login', $payload);

        $response->assertStatus(200);
    }
}
