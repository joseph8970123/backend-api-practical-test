<?php

namespace Tests\Unit;

use App\Models\FollowedUser;
use App\Models\User;
use Tests\TestCase;

class FollowTest extends TestCase
{
    public function test_a_user_can_follow()
    {
        $user = User::take(1)->first();
        $user2 = User::where('id', '!=', $user->id)->take(1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $this->withHeaders(['Authorization' => 'Bearer ' . $token])
            ->json('POST', '/api/user/follow/' . $user2->id)
            ->assertStatus(200);
    }

    public function test_a_user_can_unfollow()
    {
        $user = User::take(1)->first();
        $user2 = FollowedUser::where('user_id', $user->id)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $this->withHeaders(['Authorization' => 'Bearer ' . $token])
            ->json('DELETE', '/api/user/unfollow/' . $user2->followed_user_id)
            ->assertStatus(200);
    }
}
