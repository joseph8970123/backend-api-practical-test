<?php

namespace Tests\Unit;

use App\Models\Tweet;
use App\Models\User;
use Tests\TestCase;
use Faker\Factory as Faker;
use Illuminate\Http\UploadedFile;

class TweetTest extends TestCase
{
    public function test_user_can_create_tweet()
    {
        $faker = Faker::create();

        $user = User::first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $file = UploadedFile::fake()->image('test.jpg');

        $this->withHeaders(['Authorization' => 'Bearer ' . $token])
            ->json('POST', '/api/user/tweets', [
                'content' => $faker->paragraph(),
                'attachment' => $file
            ])
            ->assertStatus(200);
    }

    public function test_user_can_update_tweet()
    {
        $faker = Faker::create();

        $user = User::first();
        $tweet = Tweet::first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $file = UploadedFile::fake()->image('test.jpg');

        $this->withHeaders(['Authorization' => 'Bearer ' . $token])
            ->json('POST', '/api/user/tweets/update-tweet/' . $tweet->id, [
                'content' => $faker->paragraph(),
                'attachment' => $file
            ])
            ->assertStatus(200);
    }

    public function test_user_can_delete_tweet()
    {
        $faker = Faker::create();

        $user = User::first();
        $tweet = Tweet::first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $this->withHeaders(['Authorization' => 'Bearer ' . $token])
            ->json('DELETE', '/api/user/tweets/' . $tweet->id)
            ->assertStatus(200);
    }
}
